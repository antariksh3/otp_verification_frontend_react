import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import Error from './shared/pages/Error';
import Home from './app/pages/Home';
import VerifyPhone from './app/pages/VerifyPhone';
import LandingPage from './app/pages/LandingPage';

function App() {
  return <Router> 
      <Switch>
        <Route path="/" exact>
          <Home/>
        </Route>
        <Route path="/verifyPhone" component={VerifyPhone}  exact>
        </Route>
        <Route path="/landingPage" component={LandingPage}  exact>
        </Route>

        {/* Handling case for all other bad requests */}
        <Route path="/error" exact>
          <Error/>
        </Route>
        <Redirect to="/error" />

      </Switch>
    </Router>;
}

export default App;
