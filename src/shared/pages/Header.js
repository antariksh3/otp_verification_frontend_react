import React from 'react';

const Header = () => {


    const headerCss = {  
        display: 'inline-block', 
        width: '100%',
        textalign: 'center',
        background: '#1abc9c',
        color: 'white',
        fontSize: 20
    }

    const divLeft = {
        float:'left',
        padding: 10,
        paddingLeft : 40
    }

    const divRight = {
        float:'right',
        padding: 10,
        paddingRight : 40
    }

    return <div style={headerCss} >
        
            <div>
                <div style={divLeft}><h2>Verify OTP</h2></div>
            </div>
            <div>
                <div style={divRight}><h2>About Us</h2></div>
            </div>
            <div style={{clear:'both'}}></div>
        </div>;
}

export default Header;
