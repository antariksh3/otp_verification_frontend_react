import React, { useState } from 'react';
import Header from '../../shared/pages/Header';
import Footer from '../../shared/pages/Footer';
import { useHistory } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import logo from '../../res/verify.gif'
const LandingPage = () => {


    const formCss = {
        paddingTop : 50,
        textAlign:'center'
    }

    const imgCenter = {
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '300px',
        hieght: '400px'

      }


    return <div >
            <Header/>
                <div >
                    <h1 style={formCss}>Verification Complete</h1>  
                    <img style={imgCenter} src={logo} alt="verified" /> 
                </div>             
            <Footer/>    
        </div>;
}

export default LandingPage;