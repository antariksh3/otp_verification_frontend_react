import React, { useState, useEffect } from 'react';
import Header from '../../shared/pages/Header';
import Footer from '../../shared/pages/Footer';
import { useHistory } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const VerifyPhone = () => {

    var messagebird = require('messagebird')('0rS3wISzWJTSVWri6VQXcSzUZ');//live
    //var messagebird = require('messagebird')('rU8yF6tn2RwKaQ8DT4iLiWBzG');//test
    const [myVerifyRequestId,setMyVerifyRequestId] = useState('');
    let history = useHistory();
    useEffect(() => {
        console.log('myVerifyRequestId UPDATED', myVerifyRequestId);
    }, [myVerifyRequestId])
    
    const notifyOtp = () => toast("The OTP you entered is Incorrect. Please generate a new OTp and try again");

    const [phone, setPhone] = useState('')
    const [otp, setOtp] = useState('')
    const [show, setShow] = useState(false);

    const formCss = {
        paddingTop : 50,
        textAlign:'center'
    }
    const inputCss = {
        height : 50,
        width : 500,
        fontSize : 35,
        textAlign: 'center'
    }
    const paraCss = {
        marginLeft : -250
    }
    const submitCss = {
        marginTop : '50px',
        width: '300px',
        height : '50px',
        background:'rgb(17,152,248)',
        color: '#ffffff',
        textAlign: 'center',
        border: 'none',
        fontFamily: 'Roboto Condensed',
        fontSize : 35
    }

    
    
    const validateAndSubmitPhone = () => {
        
        if ( !!phone && phone.length == 10 ){
            console.log("Correct Phone");
            setPhone(phone);
            setShow(true);
            var params = {
                originator: '+19052263995',
                type: 'sms'
            };
            messagebird.verify.create('+19052263995', params, function (err, response) {
                if (err) {
                    return console.log(err);
                }
                console.log('request ');
                console.log(response);
                console.log(response.id);
                setMyVerifyRequestId(response.id);
            });
        } else {
            console.log("Please Enter Valid Phone Number");
        }
    }

    const verifyOtp = () => {
        if ( !!otp ){
            console.log("entered otp: "+otp);
            console.log("entered myVerifyRequestId: "+myVerifyRequestId);
            messagebird.verify.verify(myVerifyRequestId, otp, function (err, response) {
                if (err) {
                    notifyOtp();
                    return console.log(err);
                }
                console.log('Otp Verification Response');
                console.log(response);
                console.log('Status: '+response.status);
                if(response.status === "verified"){
                    history.push('/landingPage');
                } else {
                    notifyOtp();
                }
              });
        } else {
            console.log("Invalid Otp");
        }
    }

    return <div>
            <Header/>
            <form style={formCss}>
                <ToastContainer position="top-center"/>
                <h2 style={paraCss}>Enter Phone Num </h2>
                <input type="tel" id="phone" name="phone" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" 
                onChange={event => setPhone(event.target.value)} style={inputCss} 
                maxLength={10} placeholder="(e.g. 9998887777)"/>
                <br/>
                <input style={submitCss} type="button" onClick={validateAndSubmitPhone} value="SUBMIT" />

                <br/>
                { show  && 
                    <div id="divOtp">
                        <h2 style={paraCss}>Enter Phone OTP</h2>
                        <input type="tel" id="otp" name="otp" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" 
                        onChange={event => setOtp(event.target.value)} style={inputCss} 
                        maxLength={6} placeholder="(e.g. 1234)"/>
                        <br/>
                        <input style={submitCss} type="button" onClick={verifyOtp} value="VERIFY" />
                    </div>
                }
                <br/>
                
            </form>
            <Footer/>    
        </div>;
}

export default VerifyPhone ;