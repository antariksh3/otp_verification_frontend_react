import React, { useState } from 'react';
import Header from '../../shared/pages/Header';
import Footer from '../../shared/pages/Footer';
import { useHistory } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Home = () => {

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    let history = useHistory();
    const notifyNameAndEmail = () => toast("Please Enter Valid Name and Email.");

    const formCss = {
        paddingTop : 50,
        textAlign:'center'
    }

    const inputCss = {
        height : 50,
        width : 500,
        fontSize : 35,
        textAlign: 'center'
    }
    const paraCss = {
        marginLeft : -250
    }
    const submitCss = {
        marginTop : '50px',
        width: '300px',
        height : '50px',
        background:'rgb(17,152,248)',
        color: '#ffffff',
        textAlign: 'center',
        border: 'none',
        fontFamily: 'Roboto Condensed',
        fontSize : 35
    }

    const validateAndSaveForm = () => {
        
        var validator = require('validator');
        if ( !!name && !!email && validator.isEmail(email) ){
            console.log("Name is "+name);
            console.log("Email is "+email);

            fetch("http://localhost:8080/addUser", {
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify({
                        email: email,
                        name: name
                    })
                })
                .then(res => {
                    console.log("response: ", res);
                })
                .catch(err => {
                    console.log("error:", err);
                });

                history.push('/verifyPhone');

        } else {
            console.log("Please Enter Valid Name and Email");
            notifyNameAndEmail();
        }
      
    }

    return <div>
            <Header/>
            <ToastContainer position="top-center"/>
            <form style={formCss}>
                <h2 style={paraCss}>Enter Your Name</h2>
                <input type="text" onChange={event => setName(event.target.value)}
                style={inputCss} maxLength={25} placeholder="(e.g. Mike Jones)"/>

                <br/>
                <h2 style={paraCss}>Enter Your Email</h2>
                <input type="text" onChange={event => setEmail(event.target.value)}
                style={inputCss} maxLength={25} placeholder="(e.g. jones10@gmail.com)"/>

                <br/>
                <input style={submitCss} type="button" onClick={validateAndSaveForm} value="SUBMIT" />
            </form>
            <Footer/>    
        </div>;
}

export default Home;